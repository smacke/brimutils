#!/usr/bin/env python

import argparse
import re

def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('-m', '--main', required=True, type=str)
    p.add_argument('-o', '--out', type=str, default='arxiv.tex')
    return p.parse_args()

def find_comment(line):
    i = -1
    while True:
        i = line.find('%', i + 1)
        if i == -1: break
        if i == 0: break
        if i > 0 and line[i - 1] != '\\': break
    return i

def strip_comment(line):
    i = find_comment(line)
    if i != -1:
        line = line[:i]
    return line

def find_input_commands(line):
    pattern = r'\\input\{(\S+)\}'
    texs = re.split(pattern, line)
    return [(i % 2 == 1, x) for i, x in enumerate(texs)]

def process_file(path, out):
    with open(path, 'r') as f:
        for l in f:
            l = l.strip()
            if l.startswith('%'):
                continue
            l = strip_comment(l)
            for is_input, chunk in find_input_commands(l):
                if is_input:
                    process_file(chunk + '.tex', out)
                else:
                    print >>out, chunk

def main(args):
    with open(args.out, 'w') as out:
        process_file(args.main, out)

if __name__ == '__main__':
    args = parse_args()
    main(args)
