#!/usr/bin/env python

'''
Read matrix files and plot each one.
'''

import matplotlib.pyplot as plt
import numpy as np
import sys

if __name__ == '__main__':
    for mat in sys.argv[1:]:
        plt.figure()
        plt.imshow(np.loadtxt(mat))
        plt.show()

