#!/usr/bin/env python

import argparse
import sys


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('-d', '--item-delimiter', type=str, help='Delimits items (pairs)', default=None)
    p.add_argument('-e', '--keyval-delimiter', type=str, help='Delimits key from value', default='=')
    p.add_argument('-k', '--keys', type=str, nargs='*', help='Delimits key from value', required=True)
    return p.parse_args()


def main(args):
    d = args.item_delimiter
    e = args.keyval_delimiter
    keys = args.keys
    for l in sys.stdin:
        kvs = dict([kv.split(e) for kv in l.strip().split(d) if e in kv])
        print (d or ' ').join([e.join([k, kvs[k]]) for k in keys if k in kvs])


if __name__ == '__main__':
    args = parse_args()
    main(args)
