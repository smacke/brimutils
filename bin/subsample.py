#!/usr/bin/env python

import argparse
import random
import sys


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('--rate', required=True, type=float)
    p.add_argument('--seed', type=long)
    return p.parse_args()


def subsample(it, rate, rng=random):
    '''From pystig'''
    for x in it:
        if rng.random() <= rate:
            yield x


def main(args):
    if args.seed:
        random.seed(args.seed)
    for x in subsample(sys.stdin, args.rate):
        sys.stdout.write(x)


if __name__ == '__main__':
    args = parse_args()
    main(args)
