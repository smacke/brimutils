#!/usr/bin/env python

import operator


def cmd_max(lines):
    maxs, _ = fold_columns(lines, max, -float('inf'))
    print(' '.join(str(x) for x in maxs))

def cmd_min(lines):
    mins, _ = fold_columns(lines, min, float('inf'))
    print(' '.join(str(x) for x in mins))

def cmd_sum(lines):
    sums, _ = fold_columns(lines, operator.add, 0.0)
    print(' '.join(str(x) for x in sums))

def cmd_mean(lines):
    sums, counts = fold_columns(lines, operator.add, 0.0)
    print(' '.join(str(x / n) for x, n in zip(sums, counts)))

def fold_columns(lines, op, init):
    folds = {}
    counts = {}
    for l in lines:
        for i, x in enumerate(l.split()):
            folds[i] = op(folds.get(i, init), float(x))
            counts[i] = counts.get(i, 0) + 1

    folds = [x for _, x in sorted(folds.items())]
    counts = [x for _, x in sorted(counts.items())]
    return folds, counts

def cmd_multi(fs, lines):
    lines = list(lines)
    for f in fs:
        f(lines)

if __name__ == '__main__':
    def print_usage():
        print('Usage: colstat.py <%s>' % '|'.join(CMDS.keys()))

    import sys

    CMDS = {
        'max': cmd_max,
        'min': cmd_min,
        'sum': cmd_sum,
        'mean': cmd_mean,
        }

    args = sys.argv[1:]

    if len(args) < 1:
        print_usage()
        sys.exit(-1)

    if any(cmd not in CMDS for cmd in args):
        print_usage()
        sys.exit(-1)

    cmds = [CMDS[cmd] for cmd in args]
    if len(cmds) == 1:
        cmds[0](sys.stdin)
    else:
        cmd_multi(cmds, sys.stdin)
