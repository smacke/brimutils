#!/usr/bin/env python

import argparse
import sys


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('-d', '--delimeter', type=str)
    p.add_argument('-r', '--right-align', action='store_true');
    return p.parse_args()


def main(args):
    d = args.delimeter or None
    table = [l.strip().split(d) for l in sys.stdin]
    if len(table) == 0:
        return
    widths = [max([len(cell) for cell in col]) for col in zip(*table)]
    align = '>' if args.right_align else ''
    fmt = ' '.join(['{:' + align + str(w) + 's}' for w in widths])
    for row in table:
        print fmt.format(*row)


if __name__ == '__main__':
    args = parse_args()
    main(args)
