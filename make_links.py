#!/usr/bin/env python

from __future__ import print_function
import os, fnmatch, datetime

# BE SURE TO REMEMBER COMMAS
IGNORE_PATTERNS = [
    "*~",
    ".git",
    "make_links.py",
    "sbclrc",
    "screenrc",
    "README",
    ]

os.chdir("bin")
for f in os.listdir(os.curdir):
    for p in IGNORE_PATTERNS:
        if fnmatch.fnmatch(f, p):
            continue
    bin_name = os.path.join(os.environ["HOME"], "bin", f)
    abs_f = os.path.abspath(f)
    if os.path.lexists(bin_name):
        if os.path.islink(bin_name) and os.readlink(bin_name) == abs_f:
            print("already linked: %s" % f)
        else:
            print("already exists: %s" % f)
            print("skipping %s" %f)
        continue
    print("symlink: %s -> %s" % (bin_name, abs_f))
    os.symlink(abs_f, bin_name)
