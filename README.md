brimutils
=========

A collection of fringe utilities (to complement GNU coreutils).

I typically do:

    [bash] ~$ for f in brimutils/bin/*; do echo "Installing $f" && ln -s $f ~/bin/$f; done

Directory structure:

  bin/
    Assorted pre-built utility binaries and scripts, usually written by
    me.

  ext/
    Other projects that produce utility binaries and scripts, usually
    written by others (currently empty).

  README.txt
    You are here.
